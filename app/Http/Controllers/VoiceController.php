<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twilio\Rest\Client;

class VoiceController extends Controller
{
    public function __construct() {
        // Twilio credentials
        $this->account_sid = "ACca211e6528966ec5179e940cacf2cb6a";
        $this->auth_token = "9bd9b72f2e0dd1196b6d47564e104329";

//        dd($this->account_sid );
        //The twilio number you purchased
        $this->from = "+201122710807";

        // Initialize the Programmable Voice API
        $this->client = new Client($this->account_sid, $this->auth_token);
    }


    public function initiateCall(Request $request) {
        //Lookup phone number to make sure it is valid before initiating call
        $phone_number = $this->client->lookups->v1->phoneNumbers($request->phone_number)->fetch();

        // If phone number is valid and exists
        if($phone_number) {
            // Initiate call and record call
            $call = $this->client->account->calls->create(
                $request->phone_number, // Destination phone number
                $this->from, // Valid Twilio phone number
                array(
                    "record" => True,
                    "url" => "https://demo.twilio.com/docs/classic.mp3")
            );

            if($call) {
                echo 'Call initiated successfully';
            } else {
                echo 'Call failed!';
            }
        }
    }
}
